#include<stdio.h>
#include<math.h>
float comp(float a, float b, float c, float d)
{
    float dist;
     dist=sqrt(pow((c-a),2)+pow((d-b),2));
     return dist;

}
void output(float a)
{
   printf("The distance between the two points is %f", a);
}
int main()
{
    float x1, x2, y1, y2, dist;
    printf("Enter x,y co ordinates of first point:\n");
    scanf("%f, %f", &x1,&y1);
    printf("Enter x,y co ordinates of second point:\n");
    scanf("%f, %f", &x2, &y2);
    dist=comp(x1,y1,x2,y2);
    output(dist);
    return 0;

}