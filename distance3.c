#include<stdio.h>
#include<math.h>
struct point
{
    float x,y;
};
struct point inp()
{
    struct point p;
    printf("Enter (x,y) co ordinates of the point: ");
    scanf("%f, %f", &p.x, &p.y);
    return p;
}
float comp(struct point p, struct point q)
{
    float d;
    d=sqrt(pow((q.x-p.x),2)+pow((q.y-p.y),2));
    return d;
}
void disp(float a,struct point p, struct point q)
{
    printf("The distance between the points (%f, %f) and (%f, %f) is %f", p.x,p.y,q.x,q.y, a);
}
int main()
{
    struct point p1,p2;
    float dist;
    p1=inp();
    p2=inp();
    dist=comp(p1, p2);
    disp(dist, p1, p2);
    return 0;
}
