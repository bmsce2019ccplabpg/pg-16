#include<stdio.h>
#include<math.h>
int main()
{
    float x1, x2, y1, y2, dist;
    printf("Enter x,y co ordinates of first point:\n");
    scanf("%f, %f", &x1,&y1);
    printf("Enter x,y co ordinates of second point:\n");
    scanf("%f, %f", &x2, &y2);
    dist=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    printf("The distance between the two points is %f", dist);
    return 0;

}
