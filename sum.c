#include<stdio.h>
void inp(int a[], int n)
{
    int i;
    printf("Enter the numbers:\n");
    for(i=0;i<n;i++)
    {
        printf("%d. ",i+1);
        scanf("%d", &a[i]);

    }
}
int comp(int a[],int n)
{
    int i,s=0;
    for(i=0;i<n;i++)
        s+=a[i];
    return s;
}
void disp(int sum, int a[], int n)
{
    int i;
    
    for (i=0;i<n-1;i++)
    {
        printf("%d + ", a[i]);
    }
    
    printf("%d = %d ",a[n-1],sum);
        
}
int main()
{
    int n,sum;
    printf("Enter the number of numbers you want to add: ");
    scanf("%d", &n);
    int a[n];
    inp(a,n);
    sum=comp(a,n);
    disp(sum,a,n);
    return 0;
}