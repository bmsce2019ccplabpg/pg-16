#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number: ");
    scanf("%d", &x);
    return x;
}
int sum(int a, int b)
{
    int x;
    x=a+b;
    return x;
}
void out(int a)
{
    printf("The sum is %d", a);
}
int main()
{
    int a,b,c;
    a=inp();
    b=inp();
    c=sum(a,b);
    out(c);

    return 0;
}